'use strict';

angular.module('TrimetPDX.services.AllRoutes', [])
  .factory('getAllRoutes', function ($http) {
    return function(done) {
      $http({method: 'GET', url: 'http://developer.trimet.org/ws/V1/routeConfig/json/true/appid/9A9CB30932BAA9A65A852A135'})
        .success(function(data, status, headers, config) {
          done(data.resultSet.route);
        })
        .error(function(data, status, headers, config) {
          throw new Error('Unable to get nearby stops');
        });
    };
  });
