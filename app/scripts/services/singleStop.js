'use strict';

angular.module('TrimetPDX.services.SingleStop', [])
  .factory('getSingleStop', function ($http) {
    return function(stopID, done) {
      $http({method: 'GET', url: 'http://developer.trimet.org/ws/v2/arrivals/json/true/locIDs/'+ stopID +'/appid/9A9CB30932BAA9A65A852A135'})
        .success(function(data, status, headers, config) {
          done(data.resultSet);
        })
        .error(function(data, status, headers, config) {
          throw new Error('Unable to get stop '+ stopID +' information');
        });
    };
  });
