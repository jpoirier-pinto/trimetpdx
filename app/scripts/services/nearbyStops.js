'use strict';

angular.module('TrimetPDX.services.NearbyStops', [])
  .factory('getNearbyStops', function ($http) {
    return function(lat, lng, done) {
      $http({method: 'GET', url: 'http://developer.trimet.org/ws/V1/stops/ll/'+ lat +','+ lng +'/meters/402/json/true/appid/9A9CB30932BAA9A65A852A135'})
        .success(function(data, status, headers, config) {
          done(data.resultSet);
        })
        .error(function(data, status, headers, config) {
          throw new Error('Unable to get nearby stops');
        });
    };
  });
