'use strict';

angular.module('TrimetPDX.controllers.Route', [
  'TrimetPDX.services.RouteInfo'
])
  .controller('RouteCtrl', function ($scope, $stateParams, getRouteInfo) {
    try {
      getRouteInfo($stateParams.routeId, function(details) {
        $scope.details = details[0];
        $scope.directions = details[0].dir;
      });
    } catch(e) {
      window.error = e;
      console.error(e);
    }

    $scope.toggleDirection = function(dir) {
      if ($scope.isDirectionShown(dir)) {
        $scope.shownDirection = null;
      } else {
        $scope.shownDirection = dir;
      }
    };

    $scope.isDirectionShown = function(dir) {
      return $scope.shownDirection === dir;
    };
  });
