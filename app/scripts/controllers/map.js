'use strict';

angular.module('TrimetPDX.controllers.Map', [
  'TrimetPDX.services.Geolocation',
  'TrimetPDX.services.NearbyStops'
])
  .controller('MapCtrl', function ($scope, getCurrentPosition, getNearbyStops) {
    angular.extend($scope, {
      center: {},
      markers: {},
      defaults: {
        scrollWheelZoom: false
      }
    });

    try {
      getCurrentPosition(function(position){
        $scope.center = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
          zoom: 16
        };
        getNearbyStops(
          position.coords.latitude,
          position.coords.longitude,
          function(data){
            var allMarkers = data.location;
            allMarkers.push({
              lat: position.coords.latitude,
              lng: position.coords.longitude,
              message: 'You are here.',
              focus: true
            });
            $scope.markers = allMarkers;
          });
      });
    } catch(e) {
      window.error = e;
      console.error(e);
    }
  });
