'use strict';

angular.module('TrimetPDX.controllers.Details', [
  'TrimetPDX.services.SingleStop'
])
  .controller('DetailsCtrl', function ($scope, $stateParams, getSingleStop) {
    try {
      getSingleStop($stateParams.locId, function (data) {
        $scope.location = data.location[0];
        $scope.arrivals = data.arrival;
      });
    } catch(e) {
      window.error = e;
      console.error(e);
    }

    $scope.msToMins = function (index) {
      var arrivalTime,
          timeNow = Date.now();

      switch($scope.arrivals[index].status) {
        case 'estimated':
          arrivalTime = $scope.arrivals[index].estimated - timeNow;
          arrivalTime = parseInt((arrivalTime/(1000*60))%60) + ' minutes';
          break;
        default:
          arrivalTime = $scope.arrivals[index].scheduled - timeNow;
          arrivalTime = parseInt((arrivalTime/(1000*60))%60) + ' minutes';
          break;
      }
      return arrivalTime;
    };
  });
