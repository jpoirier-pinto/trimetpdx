'use strict';

angular.module('TrimetPDX.controllers.List', [
  'TrimetPDX.services.AllRoutes'
])
  .controller('ListCtrl', function ($scope, getAllRoutes, getRouteInfo) {
    try {
      getAllRoutes(function(routes){
        $scope.routes = routes;
      });
    } catch(e) {
      window.error = e;
      console.error(e);
    }
  });
