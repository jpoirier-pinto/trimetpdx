'use strict';

angular.module('TrimetPDX.controllers.Main', [
  'TrimetPDX.services.SingleStop'
])
  .controller('MainCtrl', function ($scope, getSingleStop) {

    $scope.getStopInfo = function (id) {
      getSingleStop(id, function (data) {
        $scope.location = data.location[0];
        $scope.arrivals = data.arrival;
        $scope.timeNow = Date.now();
      });
    };

    $scope.msToMins = function (duration) {
      return parseInt((duration/(1000*60))%60) + ' minutes';
    };

  });
