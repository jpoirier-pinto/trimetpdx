'use strict';

angular
  .module('TrimetPDX', [
    'ionic',
    'ui.router',
    'leaflet-directive',
    'ngResource',
    'TrimetPDX.controllers.Main',
    'TrimetPDX.controllers.Map',
    'TrimetPDX.controllers.List',
    'TrimetPDX.controllers.Route',
    'TrimetPDX.controllers.Details',
  ])
  .config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .state('map', {
        url: '/map',
        templateUrl: 'views/map.html',
        controller: 'MapCtrl'
      })
      .state('list', {
        url: '/list',
        templateUrl: 'views/list.html',
        controller: 'ListCtrl'
      })
      .state('route', {
        url: '/route/:routeId',
        templateUrl: 'views/route.html',
        controller: 'RouteCtrl'
      })
      .state('details', {
        url: '/route/:routeId/:locId',
        templateUrl: 'views/details.html',
        controller: 'DetailsCtrl'
      });
  });
