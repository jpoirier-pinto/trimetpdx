'use strict';

describe('Service: trimet', function () {

  // load the service's module
  beforeEach(module('TrimetPDXApp'));

  // instantiate service
  var trimet;
  beforeEach(inject(function (_trimet_) {
    trimet = _trimet_;
  }));

  it('should do something', function () {
    expect(!!trimet).toBe(true);
  });

});
